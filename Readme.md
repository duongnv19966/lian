# LIAN CHAT
Kiến trúc : MVP Java core - Android

Android studio >=2.3 



Package Name : com.skynet.lian

## Thư viện hỗ trợ :
Tất cả thư viện đã liệt kê trong app/build.gradle .

+ Image : Picasso , Glide , Crop, CircleImage...
+ Kết nối RestApi : Retrofit2 & Gson
+ Socket : Socket IO client
+ Log/Utils/Toast... : utilcode
+ View : ButterKnife Binding view, Recyclerview pull to refresh

	//    UI -  Các thư viện bổ sung view phục vụ cho hiển thị
    implementation 'com.jakewharton:butterknife:9.0.0-SNAPSHOT'						// Binding View
    annotationProcessor 'com.jakewharton:butterknife-compiler:9.0.0-SNAPSHOT'
    implementation 'me.relex:circleindicator:1.2.2@aar'						// Indicate profile slide
    implementation 'com.android.support:multidex:1.0.3'					
    implementation 'com.afollestad.material-dialogs:core:0.9.0.1'			// Dialog
    implementation 'com.squareup.picasso:picasso:2.5.2'						// Picasso load image
    implementation 'de.hdodenhof:circleimageview:2.1.0'					// ImageView Custom
    implementation 'com.makeramen:roundedimageview:2.3.0'				// ImageView Custom
    implementation 'uk.co.chrisjenx:calligraphy:2.3.0'					// FONT
    implementation 'com.jude:rollviewpager:1.3.2'						// SLIDER
    implementation 'com.android.support:cardview-v7:28.0.0'
    implementation 'net.cachapa.expandablelayout:expandablelayout:2.9.2'
    implementation 'com.theartofdev.edmodo:android-image-cropper:2.7.0'
    implementation 'com.jaeger.statusbarutil:library:1.5.0'				// status bar color
    implementation 'com.androidadvance:topsnackbar:1.1.1'
    implementation 'com.skyfishjy.ripplebackground:library:1.0.1'	
    implementation 'com.mikhaellopez:circularprogressbar:2.0.0'
    implementation 'com.sasank.roundedhorizontalprogress:roundedhorizontalprogress:1.0.1'
    implementation 'cn.lightsky.infiniteindicator:library:1.2.2'
    implementation 'com.github.JakeWharton:ViewPagerIndicator:2.4.1'			// Viewpager custom
    implementation 'q.rorbin:badgeview:1.1.0'									// badget thông báo
    implementation 'com.github.siyamed:android-shape-imageview:0.9.+@aar'			// shape view
    implementation 'com.github.ittianyu:BottomNavigationViewEx:2.0.2'				// bottombar
    implementation 'com.jsibbold:zoomage:1.2.0-SNAPSHOT'				// Zoom image chat
    implementation 'br.com.instachat:emoji-library:1.0.8'				// emoji chat
    implementation 'com.wang.avi:library:2.1.3'					// loading view
    implementation 'com.github.adrielcafe:AndroidAudioRecorder:0.3.0'			// Audio record
    implementation 'com.github.uin3566:AllAngleExpandableButton:v1.2.0'			// Expand layout
    implementation 'com.jcodecraeer:xrecyclerview:1.5.9'					// Loadmore recyclerview
    implementation 'com.github.halilozercan:BetterVideoPlayer:kotlin-SNAPSHOT'				// Video player
    implementation 'com.mindorks:paracamera:0.2.2'					// Camera
    implementation 'com.sandrios.android:sandriosCamera:1.1.0'				
    implementation 'com.karumi:dexter:5.0.0'

    //    Net -  Các thư viện về network 
    implementation('com.github.nkzawa:socket.io-client:0.6.0') {
        exclude group: 'org.json', module: 'json'
    }
    implementation 'com.squareup.retrofit2:converter-gson:2.2.0'
    implementation 'com.squareup.retrofit2:retrofit:2.3.0'
    implementation 'com.squareup.retrofit2:adapter-rxjava2:2.2.0'
    implementation 'com.google.code.gson:gson:2.8.1'
    implementation 'com.tonyodev.fetch2rx:fetch2rx:2.2.0-RC6'
    implementation 'com.squareup.okhttp3:okhttp:3.12.1'
    //    Utils - Các thư viện dùng chung
    implementation 'com.blankj:utilcode:1.5.1'
    implementation 'com.nbsp:library:1.8'
    // Pick photo -  Các thư viện sử dụng việc đọc IO - Photo/file
    implementation 'com.tbruyelle.rxpermissions2:rxpermissions:0.9.1@aar'
    implementation 'io.reactivex.rxjava2:rxjava:2.0.5'
    implementation 'io.reactivex:rxjava:1.0.+'
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.0'
    implementation 'com.github.bumptech.glide:glide:4.1.1'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.1.1'
    implementation 'me.iwf.photopicker:PhotoPicker:0.9.12@aar'
    implementation 'com.zhihu.android:matisse:0.5.2-beta4'
    // - Các thư viện về Google service
    implementation 'com.google.android.gms:play-services-places:15.0.1'
    implementation 'com.google.android.gms:play-services-location:15.0.1'
    implementation 'com.google.firebase:firebase-core:16.0.0'
    implementation 'com.google.android.gms:play-services-maps:15.0.1'
    implementation 'com.android.support:exifinterface:28.0.0'
    implementation 'com.yalantis:eqwaves:1.0.1'
 
    implementation('com.crashlytics.sdk.android:crashlytics:2.9.6@aar') {
        transitive = true;
    }
    implementation 'com.google.firebase:firebase-messaging:17.0.0'


## Kiến trúc project : 
Các file class được chia theo từng loại trong từng folder theo từng chức năng.
Mỗi package là một module tính năng bao gồm 3 phần View, Presenter, Contract, Implement theo kiến trức MVP 
+ api : chưa các class liên quan đến việc kết nối webserver.
+ Views: các class custom view
+ Utils : Các class dùng lại ở nhiều project....
+ Socket : các class liên quan xử lý socket đặt chuyến, bắt chuyến, thông báo....
+ Models : các class models của cả project, 
## Chức Năng API
Công nghệ sử dụng Retrofit 

Đường dẫn chính : app/src/main/java/com/skynet/lian/network/api
+ ApiService : Chứa toàn bộ URL, CODE Response, để kết nối RestApi
+ ApiUtils : File tạo kết nối đến Api của Restrofit

Tất cả callback sẽ chỉ trả về khi thành công và nếu lỗi xảy ra được sử lý trong file này.
+ PlaceDetailJson : Các file hỗ trợ parse Json từ google/place. 
+ ApiResponse : File class trả về chuẩn của mỗi api, Tất cả api trả về đều có dạng
```Json 
    {
     "data": {Json},
     "errorId": 200,
     "message": "OK"
     }
```
Tất cả data response sẽ tự động được cast về theo dạng ApiResponse.class bằng Gson. 
Chỉ cần định nghĩa ApiResponse<T> khi gọi callBack api.

#### Example
Gọi API Login : 
Url : ApiConstants.API_LOGIN
Method : GoiXeOmApi
```Java 
    @GET(ApiConstants.API_LOGIN)
            Call<ApiResponse<User>> login(@Query("phone") String phone, @Query("password") String password);
``` 
Tại đây đối tượng trả về là 1 User và được cast vào ApiResponse để tự động khi callbackCustom được gọi.

Gọi API :
```Java 
              Call<ApiResponse<User>> login = getmApi().login(ApiConstants.API_KEY, edtPhone.getText().toString(), password, PhoneUtils.getIMEI());
                    login.enqueue(new CallBackCustom<ApiResponse<User>>(LoginActivity.this, getDialogProgress(), new OnResponse<ApiResponse<User>>() {
                        @Override
                        public void onResponse(ApiResponse<User> response) {
                                // Xu ly khi response.getData() 
                         }
                           }));
                                }
                     });
``` 

## Socket IO Client 

Công nghệ sử dụng SocketIO java
Đường dẫn chính : app/src/main/java/com/skynet/lian/network/socket
+ SocketClient : Là 1 service chạy ngầm, tự động kết nối tới socket và thực hiện.
+ AutoRestartService : Receiver tự động khởi động lại service SocketClient để socket luôn chạy cùng hệ thống kể cả thi tắt app
+ InternetReceiver :  Receiver tự động bắt sự kiện khi mất kết nối hoặc có kết nối internet
+ SocketConstants : class chứa toàn bộ url, trạng thái chuyến đi, chuỗi config với socket
+ SocketResponse : tương tự như class ApiResponse

#### Example :
Khởi tạo socket : SocketClient.initSocket()

Hàm này thực hiện việc khởi tạo socket và các trạng thái socket, Được gọi ngay khi service chạy

+ Để đẩy 1 gói tin lên server : getSocket().emit()..

+ Để lắng nghe 1 luồng sự kiện server bắn về : getSocket().on("Ten_Luong_Su_Kien)..

Cụ thể tham khảo API DOC.

    
    

  


