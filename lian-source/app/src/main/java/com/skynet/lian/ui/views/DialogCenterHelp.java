package com.skynet.lian.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.skynet.lian.R;

import androidx.annotation.NonNull;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogCenterHelp extends Dialog {

    private Context mContext;
    private DialogCenterHelpClickListener mListener;

    public DialogCenterHelp(@NonNull Context context,DialogCenterHelpClickListener listener) {
        super(context);
        this.mListener = listener;
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_center_help);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
    }





    @OnClick({R.id.tvEmail, R.id.tvPhone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvEmail:
                mListener.onEmailClick();
                break;
            case R.id.tvPhone:
                mListener.onPhoneClick();

                break;
        }
        dismiss();
    }

    //callback

    public interface DialogCenterHelpClickListener {
        void onEmailClick();

        void onPhoneClick();
    }


    public void setDialogOneButtonClick(DialogCenterHelpClickListener listener) {
        this.mListener = listener;
    }
}
