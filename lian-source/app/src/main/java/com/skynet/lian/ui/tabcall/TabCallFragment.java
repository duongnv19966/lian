package com.skynet.lian.ui.tabcall;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.lian.R;
import com.skynet.lian.interfaces.ICallback;
import com.skynet.lian.models.CallItemModel;
import com.skynet.lian.ui.base.BaseFragment;
import com.skynet.lian.ui.views.SimpleSectionedRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class TabCallFragment extends BaseFragment implements ICallback {

    @BindView(R.id.imgHome)
    CircleImageView imgHome;
    @BindView(R.id.imgStausToolbar)
    ImageView imgStausToolbar;
    @BindView(R.id.tvNameToolbar)
    TextView tvNameToolbar;

    @BindView(R.id.imgMore)
    ImageView imgMore;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.view)
    View view;

    @BindView(R.id.textView7)
    TextView textView7;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.rcvChatItem)
    RecyclerView rcvChatItem;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.swipeTabChat)
    SwipeRefreshLayout swipeTabChat;

    @BindView(R.id.imgAddNewMessage)
    ImageButton imgAddNewMessage;
    @BindView(R.id.layoutToolbar)
    ConstraintLayout layoutToolbar;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.layoutSearch)
    ConstraintLayout layoutSearch;

    private List<CallItemModel> list;
    private AdapterCallItem adapterChatItem;

    public static TabCallFragment newInstance() {

        Bundle args = new Bundle();

        TabCallFragment fragment = new TabCallFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void doAction() {

    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_tab_call;
    }

    @Override
    protected void initViews(View view) {
        ButterKnife.bind(this, view);
        rcvChatItem.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvChatItem.setHasFixedSize(true);
//        rcvChatItem.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        tvNameToolbar.setText("Gọi điện");
        imgMore.setVisibility(View.GONE);


    }

    @Override
    protected void initVariables() {
        list = new ArrayList<>();

        adapterChatItem = new AdapterCallItem(getContext(), list);
        //This is the code to provide a sectioned list
        List<SimpleSectionedRecyclerViewAdapter.Section> sections =
                new ArrayList<SimpleSectionedRecyclerViewAdapter.Section>();

        //Sections
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(0, "Hôm nay"));
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(3, "Hôm qua"));
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(6, "21 Tháng 12, 2018"));
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(9, "21 Tháng 12, 2018"));
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(13, "21 Tháng 12, 2018"));

        //Add your adapter to the sectionAdapter
        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
        SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new
                SimpleSectionedRecyclerViewAdapter(getContext(), R.layout.section, R.id.section_text, adapterChatItem);
        mSectionedAdapter.setSections(sections.toArray(dummy));

        //Apply this adapter to the RecyclerView
        rcvChatItem.setAdapter(mSectionedAdapter);

//        new Handler().post(new Runnable() {
//            @Override
//            public void run() {
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//                list.add(new CallItemModel());
//
//
//                adapterChatItem.notifyDataSetChanged();
//            }
//        });
    }

    @OnClick({R.id.imgHome, R.id.tvNameToolbar, R.id.imgMore, R.id.imgBack, R.id.imgSearch, R.id.imgAddNewMessage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgHome:
                break;

            case R.id.tvNameToolbar:
                break;

            case R.id.imgMore:
                break;
            case R.id.imgBack:
                layoutToolbar.setVisibility(View.VISIBLE);
                layoutSearch.setVisibility(View.INVISIBLE);
                break;
            case R.id.imgSearch:
                layoutToolbar.setVisibility(View.INVISIBLE);
                layoutSearch.setVisibility(View.VISIBLE);
                break;
            case R.id.imgAddNewMessage:
                break;
        }
    }

    @Override
    public void onCallBack(int pos) {

    }
}
