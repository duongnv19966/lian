package com.skynet.lian.ui.chatgroup.editgroup.choosecontact;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.lian.R;
import com.skynet.lian.models.Profile;
import com.skynet.lian.ui.base.BaseActivity;
import com.skynet.lian.ui.searchfriend.AdapterContactItem;
import com.skynet.lian.ui.searchfriend.SearchContactContract;
import com.skynet.lian.ui.searchfriend.SearchContactPresenter;
import com.skynet.lian.ui.views.ProgressDialogCustom;
import com.skynet.lian.utils.AppConstant;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchForChooseFriendActivity extends BaseActivity implements SearchContactContract.View, com.skynet.lian.ui.contact.AdapterContactItem.CallBackContact {
    private static final int DIVIDER_SIZE = 2;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.constraintLayout2)
    ConstraintLayout constraintLayout2;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.rcv)
    RecyclerView rcv;
    @BindView(R.id.imageView9)
    ImageView imageView9;
    @BindView(R.id.tablelayout)
    TableLayout tablelayout;
    @BindView(R.id.constraintLayout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.number1)
    LinearLayout number1;
    @BindView(R.id.number2)
    LinearLayout number2;
    @BindView(R.id.number3)
    LinearLayout number3;
    @BindView(R.id.number4)
    LinearLayout number4;
    @BindView(R.id.number5)
    LinearLayout number5;
    @BindView(R.id.number6)
    LinearLayout number6;
    @BindView(R.id.number7)
    LinearLayout number7;
    @BindView(R.id.number8)
    LinearLayout number8;
    @BindView(R.id.number9)
    LinearLayout number9;
    @BindView(R.id.numberstar)
    LinearLayout numberstar;
    @BindView(R.id.number0)
    LinearLayout number0;
    @BindView(R.id.numberT)
    LinearLayout numberT;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private ProgressDialogCustom dialogLoading;
    private SearchContactContract.Presenter presenter;
    private Timer timer;
    private List<Profile> list;
    private AdapterContactItem adapter;

    @Override
    protected int initLayout() {
        return R.layout.activity_search_contact;
    }

    @Override
    protected void initVariables() {
        dialogLoading = new ProgressDialogCustom(this);
        presenter = new SearchContactPresenter(this);
        list = new ArrayList<>();
        adapter = new AdapterContactItem(this, list, this);
        rcv.setAdapter(adapter);

        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // do your actual work here
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                list.clear();
                                adapter.notifyDataSetChanged();
                                presenter.getListContact(s.toString());
                            }
                        });
                    }
                }, 600);
            }
        });
    }

    @Override
    protected void initViews() {
        rcv.setLayoutManager(new LinearLayoutManager(this));
        rcv.setHasFixedSize(true);

    }


    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.imgBack, R.id.imgSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.imgSearch:
                if (list.size() == 1) {
                    presenter.addFriend(list.get(0).getId());
                }
                break;
        }
    }

    @Override
    public void onSucessAddFriend() {
        showToast("Đã thêm bạn thành công!", AppConstant.POSITIVE);
        setResult(RESULT_OK);
    }

    @Override
    public void onSucessListContact(List<Profile> list) {
        this.list.clear();
        this.list.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hiddenProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @OnClick({R.id.number1, R.id.number2, R.id.imageView9, R.id.number3, R.id.number4, R.id.number5, R.id.number6, R.id.number7, R.id.number8, R.id.number9, R.id.numberstar, R.id.number0, R.id.numberT})
    public void onViewNumberPadClicked(View view) {
        switch (view.getId()) {
            case R.id.imageView9:
                if (edtPhone.getText().toString().isEmpty()) return;
                edtPhone.setText(edtPhone.getText().subSequence(0, edtPhone.getText().toString().length() - 1));
                break;
            case R.id.number1:
                edtPhone.setText(edtPhone.getText() + "1");
                break;
            case R.id.number2:
                edtPhone.setText(edtPhone.getText() + "2");


                break;
            case R.id.number3:
                edtPhone.setText(edtPhone.getText() + "3");

                break;
            case R.id.number4:
                edtPhone.setText(edtPhone.getText() + "4");

                break;
            case R.id.number5:
                edtPhone.setText(edtPhone.getText() + "5");

                break;
            case R.id.number6:
                edtPhone.setText(edtPhone.getText() + "6");

                break;
            case R.id.number7:
                edtPhone.setText(edtPhone.getText() + "7");

                break;
            case R.id.number8:
                edtPhone.setText(edtPhone.getText() + "8");

                break;
            case R.id.number9:
                edtPhone.setText(edtPhone.getText() + "9");

                break;
            case R.id.numberstar:
                edtPhone.setText(edtPhone.getText() + "*");

                break;
            case R.id.number0:
                edtPhone.setText(edtPhone.getText() + "0");

                break;
            case R.id.numberT:
                edtPhone.setText(edtPhone.getText() + "#");

                break;
        }
    }

    @Override
    public void onCallBack(int pos) {
        Intent i = new Intent();
        i.putExtra(AppConstant.MSG, list.get(pos).getId());
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    public void onClickInvite(int pos, Profile profile) {

    }

    @Override
    public void onClickAddfriend(int pos, Profile profile) {
        presenter.addFriend(profile.getId());
    }

    @Override
    public void onClickCall(int pos, Profile profile) {

    }

    @Override
    public void onClickVideo(int pos, Profile profile) {

    }
}
