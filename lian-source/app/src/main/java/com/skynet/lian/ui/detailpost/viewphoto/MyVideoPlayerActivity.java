package com.skynet.lian.ui.detailpost.viewphoto;

import android.net.Uri;
import android.os.Bundle;

import com.halilibo.bvpkotlin.BetterVideoPlayer;
import com.skynet.lian.R;
import com.skynet.lian.ui.base.BaseActivity;
import com.skynet.lian.utils.AppConstant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyVideoPlayerActivity extends BaseActivity {
    @BindView(R.id.player)
    BetterVideoPlayer player;

    @Override
    protected int initLayout() {
        return R.layout.activity_play_video;
    }

    @Override
    protected void initVariables() {
        player.setSource(Uri.parse(getIntent().getStringExtra(AppConstant.MSG)));
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        player.pause();

    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
